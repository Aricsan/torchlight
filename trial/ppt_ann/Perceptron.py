import numpy as np

'''
    lh -> layer hidden
    lo -> layer output
    Descenso de Gradiante (Modo Batch)
'''

class Perceptron:

    weight_lh = []      # Pesos de la capa oculta
    weight_lo = []      # Pesos de la capa de salida
    bias_lh   = []      # Bias de la capa oculta
    bias_lo   = []      # Bias de la capa de salida

    # Constructor
    def __init__ (self, size_weight_lh, size_weight_lo, size_bias_lh, size_bias_lo):

        # Iniciarlizar los pesos y bias con valores aleatorios
        self.weight_lh = np.random.randn(size_weight_lh)
        self.weight_lo = np.random.randn(size_weight_lo)
        self.bias_lh   = np.random.randn(size_bias_lh)
        self.bias_lo   = np.random.randn(size_bias_lo)

        # Transposicionar pesos a matriz de 3x3 (debido al input)
        self.weight_lh = np.array(self.weight_lh).reshape(3, 3).T
        self.weight_lo = np.array(self.weight_lo).reshape(3, 3).T

    # Función de Propagación
    def annOutput (self, input):
        input_lh = np.dot(input, self.weight_lh) + self.bias_lh        # Calcular la entrada de la capa oculta
        output_lh = self.sigmoidFunction(input_lh)                     # Calcular la salida de la capa oculta
        return np.dot(output_lh, self.weight_lo) + self.bias_lo        # Calcular la entrada de la capa de salida

    # Función Sigmoide
    def sigmoidFunction (self, input):
        return 1 / (1 + np.exp(-input))

    # Derivada de la Sigmoide
    def sigmoidDer (self, input):
        return self.sigmoidFunction(input) * (1-self.sigmoidFunction(input))

    # Fase de entrenamiento
    def training (self, inputs, target, epochs, learning_rate):

        for epoch in range(epochs):

            input_lh = np.dot(inputs, self.weight_lh) + self.bias_lh            # Calcular la entrada de la capa oculta
            output_lh = self.sigmoidFunction(input_lh)                          # Calcular la salida de la capa oculta

            input_lo = np.dot(output_lh, self.weight_lo) + self.bias_lo         # Calcular la entrada de la capa de salida
            output_lo = self.sigmoidFunction(input_lo)                          # Calcular la salida de la capa de salida

            error = output_lo - target                                          # Calcular el Error en el valor de predicción

            sumError = error.sum()                                              # Sumatorio error (comprobar la evolución)
            print ("Valor del Error: ", sumError)

            # Fase 1 (calcular los valores para los pesos de la capa de salida)
            der_lo_input = self.sigmoidDer(input_lo)                            # Calcular la derivada de la entrada de la capa oculta
            der_lo_error = np.dot(output_lh, error * der_lo_input)              # Calcular la derivada del error de la salida de la capa oculta

            # Fase 2 (calcular los valores para los pesos de la capa oculta)
            der_lh_error = error * der_lo_input                                 # Calcular la derivada del error de la entrada de la capa oculta
            der_lh_weight_error = np.dot(der_lh_error, self.weight_lo)          # Calcular el error de los pesos de la capa oculta
            der_lh_input = self.sigmoidDer(input_lh)                            # Calcular la derivada de la entrada de la capa oculta
            der_lh_final = np.dot(inputs, der_lh_input * der_lh_weight_error)   # Calcular la derivada final de la capa oculta

            bias_lh = error * self.sigmoidDer(self.sigmoidFunction(output_lh))  # Calcular la derivada del bias de la capa oculta
            bias_lo = error * self.sigmoidDer(self.sigmoidFunction(output_lo))  # Calcular la derivada del bias de la capa de salida

            # Actualizar pesos
            self.weight_lh -= learning_rate * der_lh_final
            self.weight_lo -= learning_rate * der_lo_error

            # Actualizar los valores del sesgo (bias)
            for i in bias_lh:
                self.bias_lh -= learning_rate * i
            for i in bias_lo:
                self.bias_lo -= learning_rate * i

    # Calcular predicciones
    def predict (self, input):
        #print(self.sigmoidFunction(self.annOutput(input)))
        return self.sigmoidFunction(self.annOutput(input))
