from Perceptron import Perceptron
from random import choice
from Reglas import Reglas

'''
    Descripcion -> Creación de un Modelo de Inteligencia Artificial capaz de aprender a jugar a Piedra, Papel, Tijera
    Tipo de Modelo -> Perceptrón Multicapa
    Tipo de aprendizaje -> Supervisado
    Capas del Modelo -> 3 Capas: 3 Neuronas en la capa de entrada + neurona bias
                                 3 neuronas en la capa de oculta  + neurona bias
                                 3 neuronas en la capa de salida
    Versión -> v2.0
'''

if __name__ == '__main__':

    # Construir Dataset y Target
    game = Reglas()

    # Construir el modelo
    ann = Perceptron(9,9,3,3)

    #Entrenamiento
    ann.training(game.inputs, game.gTruth, 100000, 0.1)

    # Pesos y Predicciones
    print("\nPesos Finales Capa Oculta:\n", ann.weight_lh)
    print("\nPesos Finales Capa Salida:\n", ann.weight_lo)
    print("\nValor del Bias Capa Oculta:\n", ann.bias_lh)
    print("\nValor del Bias Capa Salida:\n", ann.bias_lo)
    print("\nPredicciones:")

    for i in range(10):
        playerOption = choice(game.inputs)
        print(" Jugador: %s; Modelo: %s" % (game.mapPlayerOption(playerOption), game.mapAnnOption(ann.predict(playerOption))))
