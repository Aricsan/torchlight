#ifndef COMMON_H
#define COMMON_H

#define __DEBUG_SNAKE__
//#define __DEBUG_FRUIT__

#define BODY_SIZE  15
#define FRUIT_SIZE BODY_SIZE
#define ADVANCE    BODY_SIZE

#define SCENE_WIDTH  795
#define SCENE_HEIGHT 465

#define TIMER_SPEED 70


/* COLORS */
#define SNAKE_COLOR       0x60c18a
#define FRUIT_COLOR       0xf76c6f
#define BOARD_COLOR       0xefebe1
#define BOARD_LINES_COLOR 0xc6a365

enum TDirection {
    RIGHT,
    LEFT,
    UP,
    DOWN,
    TOTAL
};

#endif
