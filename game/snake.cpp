#include "snake.h"

Snake::Snake()
{
    score = 0;
    head  = SNAKE_INIT_LENGHT - 1;  // Índice del vector correspondiente a la cabeza del snake
    tail  = 0;  // Índice del vector correspondiente a la cola del snake
}

Snake::~Snake(){}

void
Snake::initFirstPositions()
{
    for (int i=0; i<SNAKE_INIT_LENGHT; i++)
        body.push_back(new BodyPart(0, 0));

    randomPosition();
}

void
Snake::randomPosition()
{
    timespec timeSeed;
    QRandomGenerator *rand;
    int snakeX, snakeY;


    timespec_get(&timeSeed, TIME_UTC);
    rand = new QRandomGenerator(timeSeed.tv_nsec);

    snakeX = (int) rand->bounded(SCENE_WIDTH  - BODY_SIZE);
    snakeY = (int) rand->bounded(SCENE_HEIGHT - BODY_SIZE);

    for(int i=0; i<SNAKE_INIT_LENGHT; i++)
    {
        body.at(i)->setX(mulOfBodySize(snakeX)+BODY_SIZE);
        body.at(i)->setY(mulOfBodySize(snakeY));
    }
}

/**
 * Inicializa el movimiento de la serpiente dependiendo de la zona donde se haya
 * generado la cabeza.
 */
void
Snake::initMovement(bool *rightDirection,
                    bool *leftDirection,
                    bool *upDirection,
                    bool *downDirection )
{
    int headX, headY;
    headX = body.at(head)->x();
    headY = body.at(head)->y();

    if ( headX < (SCENE_WIDTH / 2) )   // La mitad izquierda del graphicsview
    {
        if (headY <(SCENE_HEIGHT / 2)) // La mitad supierior de la izquierda
            *downDirection = true, *rightDirection = *leftDirection = *upDirection = false;
        else                           // La mitad inferior de la izquierda
            *upDirection = true, *rightDirection = *leftDirection = *downDirection = false;
    }
    else // La mitad derecha del graphicsview
    {
        if (headY <(SCENE_HEIGHT / 2)) // La mitad superior de la derecha
            *downDirection = true, *rightDirection = *leftDirection = *upDirection = false;
        else                           // La mitad inferior de la derecha
            *upDirection = true, *rightDirection = *leftDirection = *downDirection = false;
    }
}

int
Snake::mulOfBodySize(int num)
{
    return num - (num % BODY_SIZE);
}

int
Snake::getHead()
{
    return head;
}

/**
 * Devuelve un objeto QPoint con la posición de la cabeza del snake.
 */
QPoint
Snake::getHeadPosition()
{ 
    return QPoint(body.at(head)->x(),
                  body.at(head)->y());
}

/**
 * Método que actualiza el valor de la cola, a la nueva cabeza.
 * Suma la cabeza actual y la dirección recibida y lo almacena en la cola.
 */
void
Snake::move(QPoint direction)
{
    QPoint headPosition = body.at(head)->getPositions();  // Comprobado

    // Cola = Posición cabeza + Dirección
    body.at(tail)->setX( headPosition.x() + direction.x()*ADVANCE );
    body.at(tail)->setY( headPosition.y() + direction.y()*ADVANCE );


    head = tail;  // Cabeza = Antigua cola
    tail++;

    if ( head == body.size() ) head = 0;
    if ( tail == body.size() ) tail = 0;

#ifdef __DEBUG__
    qDebug("\nHead: %i\nTail: %i", head, tail);
    for (int i=0; i<body.size(); i++)
        qDebug("snake.at(%i) = [%f, %f]", i, body.at(i)->scenePos().x(), body.at(i)->scenePos().y());
#endif
}

/**
 * Método que incrementa el tamaño de snake en uno, insertando una posición con un objeto Body en
 * la siguiente posición a la cabeza, actualizando valores de head y tail.
 */
void
Snake::eatAndMove(QPoint direction)
{
    BodyPart *aux = new BodyPart(0, 0);

    int x = body.at(head)->x() + direction.x()*BODY_SIZE;
    int y = body.at(head)->y() + direction.y()*BODY_SIZE;

    aux->setPos(x, y);

#ifdef __DEBUG__
    qDebug("AUX->X = %f\nAUX->Y = %f\n",aux->x(), aux->y() );
#endif

    body.insert(body.begin() + (head+1), aux);
    head++;
    tail = head+1;

    if ( head == body.size() ) head = 0;
    if ( tail == body.size() ) tail = 0;

    increaseScore();

#ifdef __DEBUG__
    qDebug("\nComido");
    qDebug("\nHead: %i\nTail: %i", head, tail);
    for (int i=0; i<body.size(); i++)
        qDebug("snake.at(%i) = [%f, %f]", i, body.at(i)->x(), body.at(i)->y());
#endif
}

QVector<BodyPart*>*
Snake::getBody()
{
    return &body;
}

int
Snake::getScore()
{
    return score;
}

void
Snake::increaseScore()
{
    score++;
}

void
Snake::resetScore()
{
    score = 0;
}

int
Snake::getSize()
{
    return body.size();
}
