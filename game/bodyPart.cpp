#include "bodyPart.h"

BodyPart::BodyPart(int x, int y, QGraphicsRectItem *parent) :
    QGraphicsRectItem(0, 0, BODY_SIZE, BODY_SIZE, parent)
{
    setPos(x, y);
    formatBody();
}

BodyPart::~BodyPart() {}

/**
 * Añade estilo a la serpiente.
 */
void
BodyPart::formatBody()
{
    setBrush(QBrush(SNAKE_COLOR, Qt::SolidPattern));
    setPen(QPen(Qt::lightGray));
}
/**
 * Devuelve un QPoint con los valores X e Y de un objeto Body (posición de snake).
 */
QPoint
BodyPart::getPositions()
{
    QPoint positions;

    positions.setX(x());
    positions.setY(y());

    return positions;
}
