﻿#include "fruit.h"

Fruit::Fruit(QGraphicsEllipseItem *parent) : QGraphicsEllipseItem(parent)
{
    initializeFruit();
}

Fruit::~Fruit(){}


void
Fruit::initializeFruit()
{
    QPen   *pen   = new QPen(Qt::white);
    QBrush *brush = new QBrush(FRUIT_COLOR, Qt::SolidPattern);

    setBrush(*brush);
    setPen(*pen);
    setRect(0, 0, FRUIT_SIZE, FRUIT_SIZE);
#ifdef __DEBUG_FRUIT__
    fr = 0;
#endif
    delete pen;
    delete brush;

}
/**
 * Genera una nueva posición para la fruta, añade la nueva posición al item y
 * si esta nueva posición coincide con alguna posición de la seripiente, vuelve
 * a generar una nueva.
 */
void
Fruit::moveFruit(QVector<BodyPart*> *snake)
{
    QPoint *pos;
    do
    {
        pos = new QPoint();
        randomPos(pos);
        setPos(*pos);

#ifdef __DEBUG_FRUIT__
        if (fr == 5)
        {
            pos->setX(snake->at(0)->x());
            pos->setY(snake->at(0)->y());
            setPos(*pos);
            fr = 0;
        }
        fr++;
#endif
        delete pos;
    }
    while( samePosition(snake) );
}

/**
 * Genera una posición aleatoria y modifica los valores X e Y del QPoint que se le
 * pasa al metodo.
 */
void
Fruit::randomPos(QPoint *position)
{
    timespec timeSeed;
    QRandomGenerator *rand;
    int newFruitX, newFruitY;

    /* Seed para el random (se puede usar .tv_sec o .tv_nsec), segundos o nanosegundos */
    timespec_get(&timeSeed, TIME_UTC);
    rand = new QRandomGenerator(timeSeed.tv_nsec);

    newFruitX = rand->bounded(SCENE_WIDTH  - FRUIT_SIZE);
    newFruitY = rand->bounded(SCENE_HEIGHT - FRUIT_SIZE);

    position->setX(mulOfFruitSize(newFruitX));
    position->setY(mulOfFruitSize(newFruitY));

    delete rand;
}

/**
 * Comprueba si la posición de la fruta coincide con alguna parte del
 * cuerpo de la serpiente, si es asi, devuelve true, si no, false.
 */
bool
Fruit::samePosition(QVector<BodyPart*> *snake)
{
    int fruitX, fruitY, snakeX, snakeY;
    bool equals = false;

    fruitX = x();
    fruitY = y();

    for (int i=0; i<snake->size(); i++)
    {
        snakeX = (int) snake->at(i)->x();
        snakeY = (int) snake->at(i)->y();

        if ((snakeX == fruitX) && (snakeY == fruitY))
        {
            equals = true;

#ifdef __DEBUG_FRUIT__
            qDebug("COINCIDE\n");
            qDebug("F-X: %d\
                   \rF-Y: %d\
                   \rS-X: %d\
                   \rS-Y: %d\n",
                   fruitX, fruitY,
                   snakeX, snakeY);

            qDebug("FIN\n");
#endif

            return equals;
        }
    }
    return equals;
}

int
Fruit::mulOfFruitSize(int num)
{
    return num - (num % FRUIT_SIZE);
}
