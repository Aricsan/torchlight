#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "snake.h"
#include "fruit.h"
#include "bodyPart.h"

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGuiApplication>
#include <QBrush>
#include <QKeyEvent>
#include <QPointF>
#include <QString>
#include <QEvent>
#include <QApplication>
#include <QScreen>
#include <QTimer>



QT_BEGIN_NAMESPACE
namespace Ui { class Controller; }
QT_END_NAMESPACE

class Controller : public QMainWindow
{
    Q_OBJECT


public:
    Controller(QWidget *parent = nullptr);
    ~Controller();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_btnInit_clicked();
    void on_btnReset_clicked();
    void on_btnExit_clicked();
    void updateSnake();

private:
    void setDirections();
    void initializeGraphics();
    void moveSnake();
    void addObject(QGraphicsItem *object);
    void initializeGameObjects();
    QPointF getObjectPosition(QGraphicsItem *object);
    void getAxisValues(QGraphicsItem *object, qreal *x, qreal *y);
    void changeScoreValue();
    bool snakeCollapseFruit();
    bool hintWall();
    void debugPosition();
    void bodyToScene();
    bool cannibalism();
    void initializeMovement();
    void gameOver();
    void resetGame();
    void drawBackground();
    void setMoveDirection(bool left, bool up, bool right, bool down, const int moveDirection);
    void debugFruit();

    QTimer *timer;

    Ui::Controller *ui;
    QGraphicsScene *graphicScene;  // Maneja items 2D como un Ellipse

    /* QGraphicsItems */
    Snake *snake;
    Fruit *fruit;

    bool runningGame;

    QVector<BodyPart*>* snakePositions;

    /* Flags de la dirección actual */
    bool leftDirection,
    rightDirection,
    upDirection,
    downDirection;

    int timerId;
    QPoint directions[TOTAL];
    QPoint direction;

};
#endif
