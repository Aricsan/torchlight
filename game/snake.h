#ifndef SNAKE_H
#define SNAKE_H

#define SNAKE_INIT_LENGHT 4

#include "common.h"
#include "bodyPart.h"

#include <QVector>
#include <QPoint>
#include <QRandomGenerator>
#include <ctime>

class Snake
{

public:
    Snake();
    ~Snake();

    int getScore();
    void increaseScore();
    void resetScore();
    QVector<BodyPart*>*getBody();
    void move(QPoint direction);
    void moveAndEat();
    void eatAndMove(QPoint direction);
    QPoint getHeadPosition();
    void initFirstPositions();
    int getHead();
    int getSize();
    void initMovement(bool*, bool*, bool*, bool*);

private:
    void randomPosition();
    int mulOfBodySize(int num);

    int head, tail;
    QVector<BodyPart*> body;
    int score;
};

#endif
