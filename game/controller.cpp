#include "controller.h"
#include "ui_controller.h"

/**
 * Constructor que inicializa los gráficos y
 * los objetos (Serpiente, Fruta y Cuerpo de la Serpiente)
 */
Controller::Controller(QWidget *parent) : QMainWindow(parent) , ui(new Ui::Controller)
{
    ui->setupUi(this);

    /* Centrar Ventana */
    move(QGuiApplication::screens().at(0)->geometry().center() - frameGeometry().center());

    initializeGraphics();
    setDirections();
}


Controller::~Controller()
{
    delete ui;
}

void
Controller::drawBackground()
{
    for(int j=0; j<SCENE_HEIGHT; j+=BODY_SIZE)
        graphicScene->addLine(0, j, SCENE_WIDTH, j, QPen(BOARD_LINES_COLOR));

    for(int j=0; j<SCENE_WIDTH; j+=BODY_SIZE)
        graphicScene->addLine(j, 0, j, SCENE_HEIGHT, QPen(BOARD_LINES_COLOR));
}
/**
 * Lanza un evento de tecla pulsada QKeyEvent con la dirección actual
 * para que la serpiente se mueva al iniciar la partida.
 */
void
Controller::initializeMovement()
{
    int key;

    /* Verifica la dirección actual y se guarda la tecla que lanzará el evento */
    if(rightDirection)
        key = Qt::Key_D;

    else if(leftDirection)
        key = Qt::Key_A;

    else if(upDirection)
        key = Qt::Key_D;
    else
        key = Qt::Key_A;

    /* Crea un evento que pulsa una tecla configurada previamente */
    QKeyEvent event(QEvent::KeyPress, key, Qt::NoModifier);

    /* Lanza el evento */
    QApplication::sendEvent(this, &event);

}

void
Controller::initializeGraphics()
{
    ui->graphicsView->setMaximumSize(SCENE_WIDTH, SCENE_HEIGHT);
    ui->graphicsView->setMinimumSize(SCENE_WIDTH, SCENE_HEIGHT);
    ui->graphicsView->setBackgroundBrush(QBrush(BOARD_COLOR));
    ui->graphicsView->setCursor(Qt::BlankCursor);


    graphicScene = new QGraphicsScene(this);  // Manejador de items 2D.
    graphicScene->setSceneRect(0, 0, SCENE_WIDTH, SCENE_HEIGHT);


    ui->graphicsView->setScene(graphicScene); // Añadimos el Scene al Widget de Gráficos del UI.
    ui->graphicsView->setDragMode(QGraphicsView::NoDrag);  // Deshabilita el arrastre con el ratón
    drawBackground();

}

/**+
 * Añade cualquier tipo de item a la Scene
 */
void
Controller::addObject(QGraphicsItem *object)
{
    graphicScene->addItem(object);
}

void
Controller::setDirections()
{
    directions[0].setX(1),  directions[0].setY(0);  // Derecha
    directions[1].setX(-1), directions[1].setY(0);  // Izquierda
    directions[2].setX(0),  directions[2].setY(-1); // Arriba
    directions[3].setX(0),  directions[3].setY(1);  // Abajo
}

/**
 * Metodo que ejecuta el timer cada TIMER_SPEED milisegundos.
 *
 * Comprueba si se ha chocado con la pared.
 * Comprueba si se ha chocado con la fruta después
 * Mueve la serpiente.
 */
void
Controller::updateSnake()
{
    debugPosition();
    debugFruit();

    if ( snakeCollapseFruit() ) {
        snake->eatAndMove(direction);
        addObject(snake->getBody()->at(snake->getHead()));
        changeScoreValue();
        fruit->moveFruit(snake->getBody());

    } else
        snake->move(direction);

    if ( cannibalism() || hintWall() )
        resetGame();
}
/**
 * Evento que detecta cualquier tecla pulsada en el teclado, y comprueba que sean W, A, S o D.
 *
 * En función del movimiento actual y la tecla pulsada se configura el nuevo movimiento, es decir,
 * si se mueve a la Izquierda y se pulsa la D no se modifica el nuevo movimiento.
 */
void
Controller::keyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event);

    int key = event->key();

    if (leftDirection) {
        if  (key== Qt::Key_A)
            setMoveDirection(false, false, false, true, DOWN);

        else if (key == Qt::Key_D)
            setMoveDirection(false, true, false, false, UP);
    }

    else if (upDirection) {
        if  (key== Qt::Key_A)
            setMoveDirection(true, false, false, false, LEFT);

        else if (key == Qt::Key_D)
            setMoveDirection(false, false, true, false, RIGHT);
    }

    else if (rightDirection) {
        if  (key== Qt::Key_A)
            setMoveDirection(false, true, false, false, UP);

        else if (key == Qt::Key_D)
            setMoveDirection(false, false, false, true, DOWN);
    }

    else if (downDirection) {
        if  (key== Qt::Key_A)
            setMoveDirection(false, false, true, false, RIGHT);

        else if (key == Qt::Key_D)
            setMoveDirection(true, false, false, false, LEFT);
    }
}

void
Controller::setMoveDirection(bool left,
                             bool up,
                             bool right,
                             bool down,
                             const int moveDirection)
{
    leftDirection  = left;
    upDirection    = up;
    rightDirection = right;
    downDirection  = down;
    direction = directions[moveDirection];
}

/**
 * Verifica si la posición de la serpiente es la misima que la de la fruta.
 *
 * Si coinciden se incrementa el score de la serpiente y se mueve la fruta a una nueva
 * posición aleatorio en el tablero.
 */
bool
Controller::snakeCollapseFruit()
{
    QPoint fruitPos  = fruit->pos().toPoint();
    QPoint snakeHead = snake->getHeadPosition();

    if (snakeHead == fruitPos)
        return true;

    else return false;
}

/**
 * Añade al manejador de Items 2D (Scene) el cuerpo completo
 * de la serpiente.
 *snakeX
 * Los items solo se añaden una vez al Scene para que este
 * los gestione.
 */
void
Controller::bodyToScene()
{
    for(int i=0; i<snakePositions->size(); i++)
        graphicScene->addItem(snakePositions->at(i));
}

/**
 * Verifica si la serpiente ha chocado con la pared, y devuleve true si se ha chocado.
 */
bool
Controller::hintWall()
{
    int headX = snake->getHeadPosition().x();
    int headY = snake->getHeadPosition().y();

    if( (headX >= SCENE_WIDTH) || (headX < 0) )
        return true;

    else if ((headY >= SCENE_HEIGHT) || (headY < 0 ))
        return true;

    else return false;

}

/**
 * Comprueba si la cabeza se ha chocado con alguna parte del cuerpo, recorriendo
 * todas las partes del cuerpo y devuelve true si se ha chocado.
 */
bool
Controller::cannibalism()
{
    QPoint headPosition = snake->getBody()->at(snake->getHead())->getPositions();

    for (int i=0; i<snake->getSize(); i++)
        if (i != snake->getHead())
            if (headPosition.x() == snake->getBody()->at(i)->x() &&
                    headPosition.y() == snake->getBody()->at(i)->y() )
                return true;
    return false;
}

/**
 * Obtiene la posición de cualquier item en el Scene.
 */
void
Controller::getAxisValues(QGraphicsItem *object, qreal *x, qreal *y)
{
    *x = object->x();
    *y = object->y();
}

void
Controller::gameOver()
{
     resetGame();
}

/**
  * Se crean los objetos Snake y Fruit, se configura
  * la dirección por defecto (Derecha).
  *
  * Se añade el foco al Item Snake para que este pueda
  * capturar los eventos de las teclas pulsadas del teclado.
  *
  * Y se obtiene el body de la serpiente para añadirlo al Scene.
 */
void
Controller::initializeGameObjects()
{
    snake = new Snake();
    fruit = new Fruit();
    timer = new QTimer(this);

    runningGame = false;

    /* Cuando el timer mande la señal timeout, lo conecto con el metodo
    *  de esta misma clase, llamado updateSnake()
    */
    connect(timer, SIGNAL(timeout()), this, SLOT(updateSnake()));

    snake->initFirstPositions();
    snake->initMovement(&rightDirection,
                        &leftDirection,
                        &upDirection,
                        &downDirection);


    /* Guardamos el cuerpo de la serpiente para poder modificar sus datos en controller.cpp */
    snakePositions = snake->getBody();
    fruit->moveFruit(snakePositions);

    addObject(fruit);
    bodyToScene();

    /* Si no lanzamos el evento, la serpiente muere nada más empezar
     * a no ser que nosotros pulsemos una tecla
     */
    initializeMovement();
}
void
Controller::on_btnInit_clicked()
{
    if (!runningGame){
        initializeGameObjects();
        timer->start(TIMER_SPEED);
        runningGame = true;
    }
}

void
Controller::on_btnReset_clicked()
{
    resetGame();
}

void
Controller::resetGame()
{
    if (runningGame){
        timer->stop();

        delete snake;
        delete fruit;
        delete timer;

        graphicScene->clear();
        drawBackground();

        ui->lblSnakeX->setText("SNAKE X: 0");
        ui->lblSnakeY->setText("SNAKE Y: 0");
        ui->lblFruitX->setText("FRUIT X: 0");
        ui->lblFruitY->setText("FRUIT Y: 0");

        ui->scoreLabelNum->setText("0");

        runningGame = false;
    }

}

/* ################################################ */
/* ################################################ */
/* ##           Metodos de Debug                ##  */
/* ################################################ */
/* ################################################ */

void
Controller::changeScoreValue()
{
    QString score = QString::number(snake->getScore());
    ui->scoreLabelNum->setText(score);
}

void
Controller::debugPosition()
{
    QString x, y;
    x = QString("SNAKE X: ");
    y = QString("SNAKE Y: ");

    x += QString::number(snake->getHeadPosition().x());
    y += QString::number(snake->getHeadPosition().y());


    ui->lblSnakeX->setText(x);
    ui->lblSnakeY->setText(y);
}

void
Controller::debugFruit()
{
    QString x, y;
    x = QString("FRUIT X: ");
    y = QString("FRUIT Y: ");

    x += QString::number(fruit->x() / BODY_SIZE);
    y += QString::number(fruit->y() / BODY_SIZE);


    ui->lblFruitX->setText(x);
    ui->lblFruitY->setText(y);
}

void Controller::on_btnExit_clicked()
{
    exit(0);
}
