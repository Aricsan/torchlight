# Deep Learning with PyTorch Curses

-   [Deep Learning Una introducción práctica](https://www.youtube.com/watch?v=kVFflnP9S8Y)
    > ES
 
-   [CUDA Explained - Why Deep Learning uses GPUs](https://www.youtube.com/watch?v=6stDhEA0wFQ&list=PLZbbT5o_s2xrfNyHZsM6ufI0iZENK9xgG&index=4) 
	> EN

-   [Deep Learning and Neural Networks with Python and Pytorch](https://www.youtube.com/watch?v=BzcBsTou0C0)
    > EN

-   [PyTorch for Deep Learning - Full Course](https://www.youtube.com/watch?v=GIsg-ZUy0MY)
    > EN

-   [Applied Deep Learning with PyTorch - Full Course](https://www.youtube.com/watch?v=CNuI8OWsppg)
    > EN

-   [Deep Learning with PyTorch Live Course](https://www.youtube.com/watch?v=vo_fUOk-IKk)
    > EN
# Documentation

-   [Una presentación chula](https://slideplayer.es/slide/10571950/)
    > Espagnol

-   [Teoría Redes Neuronales](https://www.frro.utn.edu.ar/repositorio/catedras/quimica/5_anio/orientadora1/monograias/matich-redesneuronales.pdf)
    > ES

-   [Redes Neuronales Teoría UPV/EHU](http://www.sc.ehu.es/ccwbayes/docencia/mmcc/docs/t8neuronales.pdf)
    > ES

-   [Neural Networks from Scratch with Python Code and Math in Detail - I](https://medium.com/towards-artificial-intelligence/building-neural-networks-from-scratch-with-python-code-and-math-in-detail-i-536fae5d7bbf)
    > EN

-   [Building Neural Networks with Python Code and Math in Detail - II](https://medium.com/towards-artificial-intelligence/building-neural-networks-with-python-code-and-math-in-detail-ii-bbe8accbf3d1)
    > EN

-   [Main Types of Neural Networks and its Applications](https://medium.com/towards-artificial-intelligence/main-types-of-neural-networks-and-its-applications-tutorial-734480d7ec8e)
    > EN

-   [Machine Learning Algorithms For Beginners with Code Examples in Python](https://medium.com/towards-artificial-intelligence/machine-learning-algorithms-for-beginners-with-python-code-examples-ml-19c6afd60daa)
    > EN

-   [Main tutorials page](https://towardsai.net/p/category/tutorial)
    > EN
