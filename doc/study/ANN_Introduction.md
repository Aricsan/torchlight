## Definición
Las redes neuronales es la unidad funcional del aprendizaje profundo (Deep Learning) y Machine Learning.

Su objetivo es descubrir patrones en los datos, por los cuales dar una respuesta, se basan en el funcionamiento de las neuronas biologías, donde reciben información por medio de nuestros sentidos, la procesan y generan una respuesta.

Las redes neuronales artificiales (ANN) obtienen datos de entrada (inputs) y mediante cálculos, reconocen patrones en esos datos y predicen la salida.

## Funcionamiento
En una red neuronal disponemos de neuronas de entrada (siempre valores numéricos), neuronas ocultas
y neuronas de salida, (indican el resultado final). Estas neuronas están separadas por capas, capa de entrada, capa oculta y capa de salida.

El único control que tenemos es sobre las capas de entrada y salida, en la capa oculta no podremos cambiar ni modificar ningún valor, ya que es aquí es donde se ajusta de forma automática.

Por ejemplo, la siguiente red neuronal se encarga de analizar una imagen
y clasificarla dependiendo de si es un perro, un gato o un humano.

Las neuronas de entrada obtendrán las imágenes en forma de números, es
decir, el valor numérico de los píxeles.

Las capas ocultas se encargan de analizar
características como el número de patas, la cantidad de pelo, el tamaño,
la intensidad de un color... Utilizando los pesos intenta encontrar patrones en los datos que le permite realizar un predicción.

Por cada resultado existe una neurona de salida, una para el perro, otra
para el gato y otra para el humano, el resultado será un valor numérico,
si este valor coincide más con una neurona en concreto que con otra dará
como salida la que más coincidencias tenga (la que tenga el valor mas cercano a 1), si no disponemos de ninguna
coincidencia la imagen no es ni un perro, ni un gato, ni un humano.

![](ANN_Introduction.png)

## Pesos (Weights)
En el ejemplo anterior todas las neuronas están conectadas entre sí, cada
conexión es un peso, valor que se utiliza para ajustar la precisión de la predicción. 

El peso define la importancia de la característica de la conexión entre dos neuronas, siendo unas mas importantes que otras, por eso unos pesos son mayores que otros.

El valor de una neurona se obtiene multiplicando el valor de entrada o neurona anterior por el peso. El resultado siempre debe de estar entre 0 y 1 (neurona activa o no activa).

Para entrenar a esta red neuronal deberíamos de pasar la imagen de un
humano y indicarle que la neurona que debe estar activa es la del
humano. En este momento la ANN se encarga de ajustar de forma automática he
independiente cada uno de los pesos para ir poco a poco acercándose al
resultado final. 
Esto deberíamos de hacerlo con miles o millones de
muestras y lentamente con el tiempo los pesos se irán ajustando para
obtener el resultado esperado. Digamos, que la red neuronal aprende qué
valores exactos poner para cada uno de los pesos, para acercándose lo máximo posible al valor final.

Al fin y al cabo una red neuronal es como una función con millones de
variables y parámetros que la máquina puede modificar para ajustarlo a
lo que nosotros le pedimos. (Por esto la GPU es más eficiente que la CPU
para calcular redes neuronales, hay millones de pesos que ajustar)

## Machine Learning VS Deep Learning
La diferencia entre entre ML y DL esta en la representación de los datos.

Ambas tecnologías incorporan redes neuronales, sin embargo, las redes neuronales de Machine learning están limitadas por el factor humano, ya que este es el que prepara y formatea los datos que la red procesa y clasifica.
Cuando los datos son complicados empiezan las limitaciones, digamos que son redes neuronales mas sencillas, que no saben cocinar, solo comer.

Sin embargo el Deep Learning,  aparte de descubrir los patrones en los datos, también descubre la mejor representación de los mismos, estas redes neuronales formatean, analizan, procesan y clasifican los datos sin necesitar de la intervención del humano, gracias a ello son capaces de encontrar reglas que quizás nunca se nos habrían ocurrido o que ni siquiera tendrían sentido para nosotros. 

Debido a esto el Deep Learning es el modelo de aprendizaje automático mas extendido en el mundo de la Inteligencia Artificial.

![](ML_vs_DL.png)
