## Fase de Aprendizaje
La ANN (Artificial Neural Network) aprende en la denominada fase de aprendizaje, cada neurona esta conectada con las demás y cada conexión tiene un peso (weights) asociado a ella, el peso es la importancia de ese valor o característica por encima de los demás, por ejemplo:

Para saber si en una imagen hay una persona o un perro sera mas importante el valor del tamaño y el numero de extremidades que el tamaño y la posición de los ojos, esos son los pesos, representado con una w.

Aparentemente nosotros no conocemos los pesos de cada conexión, lo que se hace es asignar pesos de forma aleatoria y calcular el error de salida con la salida esperada mediante un algoritmo denominado, Descenso de Gradiante. Por medio de este algoritmo se ajustan esos pesos (incrementado o disminuyendo el valor) para obtener una salida lo mas fiel posible. Repetimos este proceso (Iteraciones o Generaciones) hasta obtener un valor de error lo mas cercano a 0.


Por lo tanto, en la fase de aprendizaje la ANN aprende ajustando los pesos para poder predecir la salida correcta para los datos de entrada.

### Pasos para el Entrenamiento en la fase de Aprendizaje
1. Obtener inputs
2. Asignar pesos aleatorios a los inputs o características de entrada
3. Ejecutar la ANN para el entrenamiento
4. Sacar el valor de error en la predicción
5. Actualizar el peso de los inputs mediante el Descenso de Gradiante
6. Repetir la fase de entrenamiento con los nuevos pesos hasta encontrar el mínimo posible
7. La ANN esta lista para realizar predicciones

#### Diagrama de entrenamiento de una ANN

<div align="center">
  <img src="entrenamiento_ANN.png">
</div>

### Descenso de gradiente
El descenso de gradiante es un algoritmo de optimización, nos permite conocer el mínimo valor en cualquier función f(x).

Nuestra red neuronal hará uso de una función para ajustar los pesos de la cual desconocemos su valor mínimo. Para ello podemos calcular la derivada (pendiente) de f(x) que nos proporciona la rapidez con que cambia una función, es decir cuando crece.


Si seguimos la dirección contraria a esta pendiente (-) iremos bajando esa pendiente hasta un punto mínimo, hemos calculado el mínimo local de la f(x). Porque local, es posible que la función tenga varios mínimos y que tengamos la mala suerte de encontrar el mínimo que no es el mínimo global como se muestra en la siguiente imagen.

<div align="center">
  <img src="minimoLG.png">
</div>

## Función Sigmoide
La Función Sigmoide sirve como función de activación en nuestra red neuronal. 

Para conocer si una neurona esta o no activa podemos usar la clasificación binaria de 0 o 1, sin embargo, en la vida real los inputs y outputs serán valores muy diferentes, la función sigmoidea convierte nuestros valores de salida entre 0 y 1, de esta manera podemos conocer si una neurona esta o no activa.


<div align="center">
  <img src="funcion_sigmoidea.png">
</div>

- f(x) = valor de salida entre 0 y 1
- e = constante matemática (2.71828...)
- x = valor de entrada

## Función de Propagación
Se encarga de trasformar los inputs de la ANN en la salida de ella. Es el sumatorio de las entradas multiplicadas por los pesos mas el valor de bias.

## Propagación hacia atrás (Backpropagation)
Al generar una salida a partir de una entrada, el valor el error individual de cada neurona se arrastra capa por capa,  obteniendo así el valor del error en  la predicción, que no es mas que la suma del error de todas las neuronas. 

En la fase de entrenamiento, a la hora de ajustar los pesos, no nos interesa el valor total del error, sino el valor del error individual de cada neurona, de esta manera podremos incrementar o disminuir el peso de cada conexión disminuyendo el error total. Aquí entra en juego las derivadas...

## Sesgo o Umbral (Bias)
El valor de una neurona es la multiplicación del valor de la anterior por el peso de la conexión.

El sesgo (bias) es un valor que se le otorga a una neurona, y controla qué tan predispuesta está la neurona de enviar un 1 o un 0 independiente de los pesos. Por lo cual, define la importancia de una neurona en la red, una neurona con un bias alto requiere un peso alto para ser activada, si el peso es alto significa que la conexión y la característica que representa la neurona es mas importante que las demás.

Al entrenar una ANN se pueden ajustar dos tipos diferentes de parámetros, los pesos y el valor en las funciones de activación. Esto no es práctico, ya que es posible que al realizar un cambio, este contrarreste a otro útil en la generación anterior. Sería más fácil si solo se ajustara uno de los parámetros. 

Para evitar cambiar los valores de la función de activación, se inventa una neurona de sesgo o bias, que siempre tiene un valor permanente, lo que permite ajustar con mas precisión los pesos (teniendo la tranquilidad de saber que es lo único que cambia).

Si tuviésemos una regla, los centímetros serian los pesos, si quisiese mas precisión, a nivel de milímetros no podría, pero gracias al sesgo puedo ajustar mas esos pesos pudiendo así tener milímetros en ese centímetro.

A nivel practico, en una función de activación, por ejemplo una función sigmoide, nos permite desplazar la inclinación del sigmoide (la curva) hacia la derecha o izquierda. Un sesgo alto hace que la neurona requiera una entrada más alta para generar una salida de 1. Un sesgo bajo lo hace más fácil.

Esta seria la función sigmoide para una red de tan solo una neurona de entrada y una de salida sin neurona de sesgo, en este caso se entrenaría modificando solo los pesos:

<div align="left" style="display: inline">
<img src="ANN_sin_sesgo.png">
<img src="sigmoide_sin_sesgo.png">
</div>

Como podemos observar todas las funciones cruzan por el punto (0,0). Por lo que independientemente del valor de los pesos me impediría tener un control sobre la salida de las neuronas.

Si quisiera que la función valiese 0 cuando **X** vale 2 no seria posible, al fin y al cabo los pesos multiplican al valor **input** por lo que controlando solo los pesos seria incapaz de ajustar la salida de la neurona, hace falta algo mas, un valor permanente, no cambiante, que me permitiese ajustar la salida a mi gusto, de esta manera, si seria posible modificando solo los pesos, jugar con la salida, ajustando así la precisión de la ANN.

Si entra la neurona de sesgo en escena:

<div align="left" style="display: inline-block">
<img src="ANN_con_sesgo.png">
<img src="sigmoide_con_sesgo.png">
</div>

Vemos como podemos desplazar la función a la derecha o izquierda jugando así con el valor de salida. Estamos cambiando el valor de la función de activación sin modificarla directamente, lo que nos ahorra un montón de problemas.

Ahora si, cuando **X** valga 2 la función valdrá 0.

## Capas Ocultas

En una red neuronal donde los datos son linealmente separables, no hace falta capas ocultas, porque el nivel de abstracción de los datos no es mas que uno, si no es **a** es **b**.

Sin embargo, en un problema donde los pasibles datos son linealmente no separables, es necesario el uso de estas capas para abstraer información mas detallada, obtener cosas concretas que nos permitan identificar características, patrones para perfeccionar esos caminos (relaciones, conexiones) y de esa manera dar una respuesta conforme.

<div align="center" style="display: inline-block">
<img src="datos_separables.jpg">
</div>

Por ejemplo, para identificar un objeto en una imagen nos hace falta fijarnos en detalles como la forma, el color, si es un ser inerte etc. Nosotros lo hacemos inconscientemente, pero la red neuronal hace uso de estas capas ocultas que se encargan de obtener toda esta información, abstraerla y determinar una salida. Digamos que cada capa oculta se encarga de obtener una información mas concreta, hasta el punto de conocer el contenido de la imagen.

Cada capa oculta representa un nivel de abstracción para los datos.

### Nº de neuronas en las capas ocultas
El numero de neuronas de las ocultas por lo general es menor que el doble de neuronas de la capa anterior, al no ser una ciencia exacta lo mejor sera siempre probar diferentes modelos con diferentes capas.

El calculo para una aproximación seria el siguiente: **nodos de entrada x (2/3) + nodos de salida**

- Cuando un modelo de red neuronal tiene mas de una capa oculta se le denomina red neuronal profunda (**Deep Neural Network**).
- Una red neuronal densa es aquella en la que todas la neuronas de una capa se conectan con todas las neuronas de la siguiente.


<!-- ## Red neuronal multicapa (Perceptron Multicapa) -->
## Tipos de aprendizaje 
Las redes neuronales se pueden dividir en tres tipos segun el tipo de aprendizaje implementado para el entrenamiento:

- Aprendizaje supervisado: consiste en que la red dispone de los patrones de entrada (dataset) y los patrones de salida o target para esas entradas. En función de estos los pesos se modifican y ajustan para la entrada a dicha salida. Es el modo de aprendizaje mas intuitivo y sencillo.

- Aprendizaje no supervisado: consiste en no presentar patrones objetivos, si no solo patrones de entrada, y dejar a la red clasificar dichos patrones en función de las características comunes entre estos. Su objetivo es categorizar (clustering) los datos que se introducen en la red. 

- Aprendizaje reforzado: el aprendizaje por refuerzo no presenta patrones objetivos, sino que consiste en mediante recompensas (score) penalizar o recompensar a la red después de tomar una decisión.

## Algoritmos de aprendizaje
Existen varios tipos de algoritmos de aprendizaje, y no todos incluyen redes neuronales. Al fin y al cabo las redes neuronales (Deep Learning) es un tipo de algoritmo de Machine Learning.

Hay infinidad de algoritmos capaces de aprender por si solos, pero el perceptron o neurona artificial es el mas revolucionario, la posibilidad de juntar varios perceptrones formando una red de neuronas interconectadas entre si es el algoritmo mas avanzado de Machine Learning hasta la fecha.

La neuroevolucion, que significa aplicar algoritmos genéticos a las redes neuronales, es de los algoritmos mas prometedores. De la misma manera que la evolución natural, consiste en generar varias redes idénticas con un porcentaje de mutaciones, las dos mejores de cada generación se reproducen, clonan con una variación (mutaciones) repetimos cada generación hasta conseguir que una aprenda y cumpla las condiciones que deseemos.